package cn.jhc.bean;

public class User {
	private int id;
	private String username;
	private String pass;
	private String phone;
	
	public User(String username, String pass, String phone) {
		super();
		this.username = username;
		this.pass = pass;
		this.phone = phone;
	}
	public User(int id, String username, String pass, String phone) {
		super();
		this.id = id;
		this.username = username;
		this.pass = pass;
		this.phone = phone;
	}
	public User() {}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", pass=" + pass + ", phone=" + phone + "]";
	}
	
}
