package cn.jhc.db;

import java.sql.SQLException;
import java.util.List;

import cn.jhc.bean.User;

public class Demo {

	public static void main(String[] args) throws SQLException {
		List<User> allUsers = DbFactory.getUserDao().find();
		allUsers.stream().forEach(user -> System.out.println(user));
	}

}
