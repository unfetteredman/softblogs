package cn.jhc.db;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;

import com.mysql.cj.jdbc.MysqlDataSource;

public class DbFactory {
	private static final String CONNECTIONURL = "jdbc:mysql://localhost/softblogs?serverTimezone=UTC";
	
	private static MysqlDataSource dataSource = null;
	private static UserDao userDao = null;
	private static ArticleDao articleDao = null;
	
	private static DataSource getDatasource() {
		if(dataSource == null) {
			dataSource = new MysqlDataSource();
			dataSource.setURL(CONNECTIONURL);
			dataSource.setUser("root");
			dataSource.setPassword("");			
		}
		return dataSource;
	}
	
	public static UserDao getUserDao() {
		if(userDao == null) {
			QueryRunner runner = new QueryRunner(getDatasource());
			userDao = new UserDao(runner);
		}
		return userDao;
	}
	
	public static ArticleDao getArticleDao() {
		if(articleDao == null) {
			QueryRunner runner = new QueryRunner(getDatasource());
			articleDao = new ArticleDao(runner);
		}
		return articleDao;
	}
}
