<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../includes/head.jsp"></jsp:include>
<script src="https://cdn.bootcss.com/moment.js/2.22.2/moment.min.js"></script>
<script src="https://cdn.bootcss.com/moment.js/2.22.2/locale/zh-cn.js"></script>
<link href="https://cdn.bootcss.com/animate.css/3.7.0/animate.min.css" rel="stylesheet"/>
<style type="text/css">
.success-message {
  color: green;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 2em;
  border: 1px solid green;
  height: 100px;
}
#input-message {
	height: 100px;
}
</style>
<title>微博首页</title>
</head>
<body>
<jsp:include page="../includes/nav.jsp"></jsp:include>
<div class="container">
	<div class="row">
		<div class="col-12 col-md-8 offset-md-2 col-xl-6 offset-xl-3">
			<div class="card">
				<div class="card-body">
					<h4 class="card-title">有什么新鲜事想告诉大家?</h4>
					<div class="p-2" id="text-container">
						<textarea rows="4" name="message" id="input_message" 
							class="w-100 border border-primary"></textarea>
					</div>
					<div class="text-right">
						<button class="btn btn-primary" disabled id="publishbtn" onclick="shuaxin()">
							发布
						</button>
					</div>
				</div>
			</div>
			<hr/>
			<div id="myarticles">
			</div>
			<button id="next">下一页</button>
		</div>
	</div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/javascripts/first.js"></script>
</body>
<script>
	function shuaxin(){
		setTimeout(function(){  window.location.href="first.jsp"; }, 1000);
	}
</script>

</html>
