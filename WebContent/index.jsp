<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="./includes/head.jsp"></jsp:include>
<title>我的博客</title>
</head>
<body>
	<div class="container">
		<ul class="nav justify-content-end">
			<li class="nav-item">
				<a class="nav-link active" href="./login.jsp">登录</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="./register.jsp">注册</a>
			</li>
		</ul>
	</div>
	<div class="row">
		<div class="col-12 col-md-7 offset-md-1">
			<div class="card m-1">
				<div class="card-body">
					<h5 class="card-title">文章1</h5>
					<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
				</div>
			</div>
			<div class="card m-1">
				<div class="card-body">
					<h5 class="card-title">文章1</h5>
					<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-3">
			<div class="card m-1">
				<div class="card-body">
					<h5 class="card-title">最热文章</h5>
					<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
				</div>
			</div>		
		</div>
	</div>
</body>
</html>