<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="./includes/head.jsp"></jsp:include>
<title>登录</title>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-6 offset-md-3 d-flex flex-column justify-content-center" style="height: 100vh">
				<form class="m-2" action="./login.do" method="post">
					<div class="form-group row">
						<label class="col-12 col-md-3">用户名</label>
						<input type="text" class="form-control col-12 col-md-9" name="username"/>
					</div>
					<div class="form-group row">
						<label class="col-12 col-md-3">密码</label>
						<input type="password" class="form-control col-12 col-md-9" name="password"/>
					</div>
					<div class="row">
						<button type="submit" class="btn btn-primary btn-block col-12 offset-md-3 col-md-9">
							登录
						</button>			
					</div>

				</form>
			</div>
		</div>
	</div>
</body>
</html>