<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../includes/head.jsp"></jsp:include>
<title>微博首页</title>
</head>
<body>
<jsp:include page="../includes/nav.jsp"></jsp:include>
<div class="container">
	<div class="row">
		<div class="col-12 col-md-8 offset-md-2 col-xl-6 offset-xl-3">
			<div class="card">
				<div class="card-body">
					<h4 class="card-title">有什么新鲜事想告诉大家?</h4>
					<div class="p-2">
						<textarea rows="4" name="message" id="input_message" 
							class="w-100 border border-primary"></textarea>
					</div>
					<div class="text-right">
						<button class="btn btn-primary" disabled id="publishbtn">
							发布
						</button>
					</div>
				</div>
			</div>
			<hr/>
			<div id="myarticles">
				<c:forEach items="${articles}" var="art">
					<div class="card">
						<div class="card-body">
							${art.content}
							<div class="text-right">
								<fmt:formatDate value="${art.publish}" pattern="yyyy-MM-dd kk:mm:ss"/>
							</div>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/javascripts/front.js"></script>
</body>
</html>