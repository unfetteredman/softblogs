<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="./includes/head.jsp"></jsp:include>
<title>用户注册</title>
</head>
<body>
	<div class="d-flex justify-content-center align-items-center"
		style="height: 100vh">
		<div class="d-inline-block">
			<h3 class="text-center">用户注册</h3>
			<form action="./register.do" method="post">
				<div class="form-group row">
					<label class="col-12 col-md-4">用户名</label> 
					<input type="text" class="form-control col-12 col-md-8" name="username" />
				</div>
				<div class="form-group row">
					<label class="col-12 col-md-4">密码</label> 
					<input type="password" class="form-control col-12 col-md-8" name="password" id="password"/>
				</div>
				<div class="form-group row">
					<label class="col-12 col-md-4">确认密码</label> 
					<input type="password" class="form-control col-12 col-md-8" id="reinput"/>
				</div>
				<div class="form-group row">
					<label class="col-12 col-md-4">手机</label> 
					<input type="text" class="form-control col-12 col-md-8" name="phone" />
				</div>
				<div class="row">
					<button type="submit" class="btn btn-primary btn-block col-12 offset-md-4 col-md-8">
						注册
					</button>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript" src="./javascripts/register.js"></script>
</body>
</html>