document.addEventListener('DOMContentLoaded', function() {
  var reinput = document.getElementById('reinput')
  var password = document.getElementById('password')
  reinput.addEventListener('blur', function() {
    if(password.value !== reinput.value) {
      alert('两次密码输入密码不一致！origin: ' + password.value + ', new: ' + reinput.value)
    }
  })
})