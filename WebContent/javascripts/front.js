document.addEventListener('DOMContentLoaded', function() {
  var msgTextarea = document.getElementById('input_message')
  var publishbtn = document.getElementById('publishbtn')
  msgTextarea.addEventListener('input', function() {
    var value = msgTextarea.value.trim()
    if(value && value.length > 0 && publishbtn.hasAttribute('disabled')) {
      publishbtn.removeAttribute('disabled')
    }
  })

  publishbtn.addEventListener('click', function() {
    var msg = msgTextarea.value
    var request = new Request('./publish.do', {
      method: 'POST',
      headers: {
        'Content-Type': 'text/plain'
      },
      body: msg
    })
    fetch(request).then(function(resp) {
      return resp.json()
    })
    .then(function(res) {
      msgTextarea.value = ''
      var myarticles = document.getElementById('myarticles')
      myarticles.insertAdjacentHTML(
    		  'afterbegin', 
    		  '<div class="card">'
    		  + '<div class="card-body">'
    		  + res.content
    		  + '<div class="text-right">'
    		  + res.publish
    		  + '</div>'
    		  + '</div>'
    		  + '</div>'
      )
    })
    .catch(function(err) {
      console.log(err)
    })
  })
})
//
//